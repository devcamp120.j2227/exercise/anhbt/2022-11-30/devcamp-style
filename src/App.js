import avatar from "./assets/images/avatar.jpg";

function App() {
  return (
    <div>
      <div>
        <div>
          <img src={avatar} alt="Avatar"/>
        </div>
        <div>
          <p>
            This is one of the best developer blogs on the planet! I read it daily to improve my skills.
          </p>
        </div>
        <div>
          <b>
            Tammy Stevens
          </b>
          &nbsp;*&nbsp;Front End Developer
        </div>
      </div>
    </div>
  );
}

export default App;
